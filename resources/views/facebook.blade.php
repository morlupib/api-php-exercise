@extends('master')

@section('user-info')
    <ul style="list-style:none">
        <li>ID: {{ $user['id'] }}</li>
        <li>Fist Name: {{ $user['first_name'] }}</li>
        <li>Last Name: {{ $user['last_name'] }}</li>
    </ul>
@endsection

@section('scripts')
    <script src="https://code.jquery.com/jquery-3.3.1.js" type="text/javascript">

    </script>
    <script type="text/javascript">
        $.ajax({
            url: '{{ route('facebook_user', [4]) }}',
            type: 'GET',
            dataType: 'json'
        })
        .done(function(data) {
            console.log(data.user);
        })
        .fail(function() {
            console.log("error");
        });

    </script>
@endsection
