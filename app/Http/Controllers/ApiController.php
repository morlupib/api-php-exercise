<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Facebook\Facebook;

class ApiController extends Controller
{
    public function getFacebookUser(Request $request, $id){
        $fb = new Facebook([
            'app_id' => env('FACEBOOK_APP_ID', ''),
            'app_secret' => env('FACEBOOK_APP_SECRET', ''),
            'default_graph_version' => 'v2.4',
            'default_access_token' => env('FACEBOOK_APP_ID', '').'|'.env('FACEBOOK_APP_SECRET', '')
        ]);
        try {
            $response = $fb->get('/'.$id.'?fields=id,first_name,last_name');
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if ($request->ajax()) {
            return response()->json(['user' => $response->getDecodedBody()]);
        }

        return view('facebook')->with('user', $response->getDecodedBody());
    }
}
